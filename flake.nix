{
  outputs = { self }: {
    nixosModules = {
      backup = import ./modules/backup.nix;
      common = import ./modules/common.nix;
      hardware = {
        linode = import ./modules/hardware/linode.nix;
        virtualbox = import ./modules/hardware/virtualbox.nix;
      };
      users = import ./modules/users.nix;
      zerotier = import ./modules/zerotier.nix;
    };
  };
}
