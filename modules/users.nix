{ ... }: {
  users.users.albert = {
    description = "Albert Koch";
    isNormalUser = true;
    extraGroups = [ "wheel" ];
  };
}
