{ config, pkgs, ... }: {
  services.borgmatic = {
    enable = true;
    settings = {
      retention = {
        keep_daily = 7;
        keep_weekly = 4;
        keep_monthly = 6;
        keep_yearly = 1;
      };
      storage = {
        encryption_passcommand = "${pkgs.coreutils-full}/bin/cat /run/secrets/borgmatic_passphrase";
        ssh_command = "ssh -i /run/secrets/borgmatic_key";
      };
    };
  };
  sops.secrets = {
    borgmatic_key = {};
    borgmatic_passphrase = {};
  };
  systemd.timers.borgmatic = {
    enable = true;
    wantedBy = ["timers.target"];
  };
}
