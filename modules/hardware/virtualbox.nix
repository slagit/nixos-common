{ ... }: {
  boot = {
    initrd.availableKernelModules = [ "ata_piix" "ohci_pci" "ehci_pci" "ahci" "sd_mod" "sr_mod" ];
    loader.grub = {
      enable = true;
      device = "/dev/sda";
    };
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
    };
    "/repos" = {
      device = "nix_desktop";
      fsType = "vboxsf";
      options = [ "uid=1000" "gid=100" ];
    };
  };
  virtualisation.virtualbox.guest.enable = true;
}
