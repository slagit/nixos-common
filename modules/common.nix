{ pkgs, ... }: let
  ssh_ca_pub = pkgs.writeText "ca.pub" ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGpblcVzzHoULrq+lSGgIKhdVCZNCC9Yj6PlBI6j4LgR slagit-vault.net ssh-client-signer
  '';
in {
  console.useXkbConfig = true;
  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    substituters = [
      "https://slagit-mail.cachix.org"
    ];
    trusted-public-keys = [
      "slagit-mail.cachix.org-1:1GhK3qyiQSs7fqcWzs3oqobPvvcrJq/cw+6b/sbTTq0="
    ];
  };
  services = {
    openssh = {
      enable = true;
      extraConfig = ''
        TrustedUserCAKeys ${ssh_ca_pub}
      '';
      settings = {
        PasswordAuthentication = false;
        PermitRootLogin = "no";
      };
    };
    xserver.xkbOptions = "ctrl:nocaps";
  };
  system.stateVersion = "22.11";
}
