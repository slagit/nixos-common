{ ... }: {
  networking.firewall.interfaces.ztly5yzqza.allowedTCPPorts = [ 22 ];
  nixpkgs.config.allowUnfree = true;
  services = {
    openssh.openFirewall = false;
    zerotierone = {
      enable = true;
      joinNetworks = [ "0cccb752f73b91d7" ];
    };
  };
}
